<?php

namespace App\Controller;

use App\Entity\Desafio2;
use App\Form\Desafio2Type;
use App\Repository\Desafio2Repository;
use Doctrine\Persistence\ManagerRegistry;
use App\Services\servicioDesafio;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;

#[Route('/desafio2')]
class Desafio2Controller extends AbstractController
{
    #[Route('/', name: 'app_desafio2_index', methods: ['GET'])]
    public function index(Desafio2Repository $desafio2Repository): Response
    {
        return $this->render('desafio2/index.html.twig', [
            'desafio2s' => $desafio2Repository->findAll(),
        ]);
    }

    #[Route('/fizz/buzz', name: 'app_desafio2_new', methods: ['GET', 'POST'])]
    public function new(Request $request, Desafio2Repository $desafio2Repository, ManagerRegistry $entityManager, servicioDesafio $srvc): Response
    {
        date_default_timezone_set("America/Sao_Paulo");

        $desafio2 = new Desafio2();
        $form = $this->createForm(Desafio2Type::class, $desafio2);
        $form->handleRequest($request);
        $numI = $form->get('numeroInicial')->getData();
        $numF = $form->get('numeroFinal')->getData();
        $servicio = $srvc->FizzBuzz($numI, $numF); 
        /* $fecha = new \DateTime($form['horaRegistro']); */
        /* var_dump($fecha); */
        /* var_dump($servicio); */
        $desafio2->setFizzBuzz(implode(" | ", $servicio));
/*         for($i = 1; $i < count($servicio); $i++){

                $desafio2->setFizzBuzz(implode(" | ", $servicio));
        } */

        if ($form->isSubmitted() && $form->isValid()) {
            $desafio2Repository->add($desafio2, true);


            return $this->redirectToRoute('app_desafio2_index', [], Response::HTTP_SEE_OTHER);
            
        }

        return $this->renderForm('desafio2/new.html.twig', [
            'desafio2' => $desafio2,
            'form' => $form,
        ]);
    }
}


/* if ($form->isSubmitted() && $form->isValid()) {
    $clientesRepository->add($cliente, true);
    $file = $request->files->get('clientes')['nombre_archivo'];


    if($file){
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        $file->move(
            $this->getParameter('brochures_directory'),
            $newFilename
        );

        $cliente->setNombreArchivo($newFilename);
        $em->persist($cliente);
        $em->flush();

    }
    return $this->redirectToRoute('app_clientes_index', [], Response::HTTP_SEE_OTHER);
} */