<?php

namespace App\Controller;

use App\Twig\DesafioExtension;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/desafio1')]
class DesafioController extends AbstractController
{
    #[Route('/fizz/buzz/', name: 'app_desafio')]
    public function index()
    {
        return $this->render('base.html.twig');
    }
}
