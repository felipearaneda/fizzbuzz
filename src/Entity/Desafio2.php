<?php

namespace App\Entity;

use App\Repository\Desafio2Repository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Desafio2Repository::class)]
class Desafio2
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $numeroInicial;

    #[ORM\Column(type: 'integer')]
    private $numeroFinal;

    #[ORM\Column(type: 'datetime')]
    private $horaRegistro;

    #[ORM\Column(type: 'string', length: 255)]
    private $FizzBuzz;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroInicial(): ?int
    {
        return $this->numeroInicial;
    }

    public function setNumeroInicial(int $numeroInicial): self
    {
        $this->numeroInicial = $numeroInicial;

        return $this;
    }

    public function getNumeroFinal(): ?int
    {
        return $this->numeroFinal;
    }

    public function setNumeroFinal(int $numeroFinal): self
    {
        $this->numeroFinal = $numeroFinal;

        return $this;
    }

    public function getHoraRegistro(): ?\DateTimeInterface
    {
        return $this->horaRegistro;
    }

    public function setHoraRegistro(\DateTimeInterface $horaRegistro): self
    {
        $this->horaRegistro = $horaRegistro;

        return $this;
    }

    public function getFizzBuzz(): ?string
    {
        return $this->FizzBuzz;
    }

    public function setFizzBuzz(string $FizzBuzz): self
    {
        $this->FizzBuzz = $FizzBuzz;

        return $this;
    }
}
