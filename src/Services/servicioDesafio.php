<?php

namespace App\Services;

class servicioDesafio{

    public function FizzBuzz($numInicial, $numFinal): array
    {
        $num = 1;
        $fizzbuzz = array();
        for($num = $numInicial; $num <= $numFinal; $num++){
            if($num % 3 == 0 && $num % 5 == 0){
                array_push($fizzbuzz, "FizzBuzz"); 
                $this->fizzbuzz = "FizzBuzz";
            }else if($num % 3 == 0){
                array_push($fizzbuzz, "Fizz");
            }else if($num % 5 == 0){
                array_push($fizzbuzz, "Buzz");
            }else{
                array_push($fizzbuzz, $num);
            }
        };
        return $fizzbuzz;
    }
}