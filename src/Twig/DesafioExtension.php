<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Services\servicioDesafio;

class DesafioExtension extends AbstractExtension
{
    
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
            new TwigFilter('filtro', [$this, 'FizzBuzz']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('funcion', [$this, 'FizzBuzz']),
        ];
    }

    public function FizzBuzz($numFinal): string
    {
        $servicio = new servicioDesafio();

        $x = $servicio->FizzBuzz(1, $numFinal);

        $conversion= implode(" | ", $x);

        return $conversion;
    }
}
/*         $num = 0;
        for($num = 1; $num <= 30; $num++){
            if($num % 3 == 0 && $num % 5 == 0){
                echo "FizzBuzz, ";
            }else if($num % 3 == 0){
                echo "Fizz, ";
            }else if($num % 5 == 0){
                echo "Buzz, ";
            }else{
                echo $num. ", ";
            }
            
        };
        return 'fin'; */